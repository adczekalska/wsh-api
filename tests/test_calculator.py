import requests
import random

base_url = 'http://18.184.234.77:8080'

def test_post_calculator():
    endpoint = '/add'
    response = requests.post(f'{base_url}/add')
    assert response.status_code == 200


def test_add_calculator():
    first_number, second_number = random.randit(1, 100), random.randit(1, 1000)
    expected_result = first_number + second_number
    body = {
        "firstNumber": first_number,
        "secondNumber": second_number
    }

    add_number_response = requests.post(f'{base_url}/add', json=body)
    assert add_number_response.status_code == 200

    result = add_number_response.json()['results']
    assert result == expected_result